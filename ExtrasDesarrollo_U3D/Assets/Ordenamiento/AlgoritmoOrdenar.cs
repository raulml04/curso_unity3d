﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrdenarComoHumano
{

    public static void Main()
    {
        List<int> numeros = new List<int>();
        numeros.Add(4);
        numeros.Add(5);
        numeros.Add(3);
        numeros.Add(1);
        numeros.Add(2);
        List<int> numOrde = new List<int>();

        Debug.Log("Lista: ");
        MostrarLista(numeros);
        Debug.Log("Menor " + Menor(numeros));
        Ordenar(numeros, numOrde);
        Debug.Log("Lista ordenada: ");
        MostrarLista(numOrde);
    }
    static void Ordenar(List<int> lista, List<int> lista2)
    {
        for (int i = 0; i < lista.Count; i = i + 0)          //for(int i=0; i<lista.Count;)    //for(; 0<lista.Count;)   //while (lista.Count > 0)
        {
            lista2.Add(Menor(lista));
            lista.Remove(Menor(lista));
        }
    }

    public static void MostrarLista(List<int> lista)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Debug.Log(lista[i] + ", ");
        }
    }
    public static int Menor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
        }
        return menor;
    }
}