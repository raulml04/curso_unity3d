﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColocarNumUI : MonoBehaviour
{
    public List<Text> numeros;
    List<int> listaNum;
    private List<int> listaNum2;
    int menorDeTodos;

    public UnityEngine.Vector2 posInicial;
    const int ANCHO_TEXTO = 100;
    static UnityEngine.Vector2 DIMENSIONES = new Vector2(ANCHO_TEXTO, 0);

    // Start is called before the first frame update
    void Start()
    {
        listaNum2 = new List<int>();
        listaNum = new List<int>();

        foreach (Text txtNum in numeros)
        {
            int num = Int32.Parse(txtNum.text);
            listaNum.Add(num);
        }

        // Es lo mismo que:
        for (int i = 0; i < numeros.Count; i++)
        {
            Text txtNum = numeros[i];
            // int num = Int32.Parse(txtNum.text); ....
        }
        menorDeTodos = OrdenarComoHumano.Menor(listaNum);
        // menorDeTodos = OrdenarComoHumano.Menor(listaNum2);
        OrdenarComoHumano.MostrarLista(listaNum);
        ColocarSeguidos();
    }
    public void ColocarSeguidos()
    {
        List<int> listaPosX = new List<int>();

        foreach (Text txtNum in this.numeros)
        {
            int posX = (int)txtNum.rectTransform.position.x;
            listaPosX.Add(posX);
        }
        int posMenorX = OrdenarComoHumano.Menor(listaPosX);
        this.posInicial = new Vector2(posMenorX, this.numeros[0].rectTransform.rect.position.y);

        this.numeros[0].rectTransform.localPosition = this.posInicial;

        /*for (int i = 1; i < this.numeros.Count; i = i + 1 ) // i++ 
        {
            // Vector2 posSiguiente = this.posInicial + DIMENSIONES;
            int posSiguiente = posMenorX + ANCHO_TEXTO;
            Vector2 vPosSig = new Vector2(posSiguiente, this.numeros[i].rectTransform.position.y);
            
        }*/
    }
}
