﻿using UnityEngine;

public class GameOfLiveScipt : MonoBehaviour
{
    public Texture2D planoSemilla;
    public Texture2D plano_2;
    public GameObject quad;
    void Start()
    {
        if (planoSemilla != null)
        {
            planoSemilla.SetPixel(planoSemilla.width - 2, planoSemilla.height - 2, Color.white);
            planoSemilla.Apply();
            plano_2 = new Texture2D(planoSemilla.width, planoSemilla.height, TextureFormat.RGB24, false);
            plano_2.anisoLevel = 0;
            plano_2.alphaIsTransparency = false;
            plano_2.Apply();

            for (int x = 0; x < plano_2.width; x++)
            {
                for (int y = 0; y < plano_2.height; y++)
                {
                    plano_2.SetPixel(x, y, Color.black);
                }
            }
            plano_2.Apply();
            quad.GetComponent<Renderer>().material.mainTexture = plano_2;
        }
        int v = CuantosVecinosVivos(planoSemilla, 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        for (int x = 0; x < plano_2.width; x++)
        {
            for (int y = 0; y < plano_2.height; y++)
            {
                plano_2.SetPixel(x, y, Color.black);
                if (planoSemilla.GetPixel(x, y) == Color.black)
                {
                    if (CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        plano_2.SetPixel(x, y, Color.white);
                    }
                }
                if (planoSemilla.GetPixel(x, y) == Color.white)
                {
                    if (CuantosVecinosVivos(planoSemilla, x, y) == 2 || CuantosVecinosVivos(planoSemilla, x, y) == 3)
                    {
                        plano_2.SetPixel(x, y, Color.white);
                    }

                }
            }
        }
        plano_2.Apply();
        /*for (int x = 0; x < plano_2.width; x++)
        {
            for (int y = 0; y < plano_2.height; y++)
            {
                planoSemilla.SetPixel(x, y, plano_2.GetPixel(x, y));
            }
        }*/

        planoSemilla.Apply();
    }

    int CuantosVecinosVivos(Texture2D tex, int px, int py)
    {
        int cuentaVivos = 0;
        for (int x = px - 1; x <= px + 1; x++)
        {
            for (int y = py - 1; y <= py + 1; y++)
            {
                if (px != x || py != y)
                {
                    if (tex.GetPixel(x, y) == Color.white)
                    {
                        cuentaVivos = cuentaVivos + 1;
                    }
                }
            }
        }
        return cuentaVivos;
    }
}
