﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorJuego : MonoBehaviour
{
    public GameObject prefabPolvos;
    public int vidas = 7;
    public int puntos = 0;
    public GameObject textoVidas;
    public GameObject textoPuntos;
    public GameObject[] enemigos;

    [HideInInspector]
    public bool recuVidas = true;
    public bool creacion = false;
    public float randomTime;
    public float puntosActual;
    

    // Start is called before the first frame update
    void Start()
    {
        GameObject.Instantiate(enemigos[Random.Range(0, enemigos.Length)]);
        StartCoroutine(Polvos());
        randomTime = Random.Range(20, 75);
        puntosActual = puntos + 50;
    }

    // Update is called once per frame
    void Update()
    {
        textoVidas.GetComponent<Text>().text = "VIDAS: " + this.vidas;
        textoPuntos.GetComponent<Text>().text = "PUNTOS: " + this.puntos;
        if (creacion == true)
        {
            GameObject.Instantiate(enemigos[Random.Range(0, enemigos.Length)]);
            creacion = false;
        }
        if (vidas == 0)
        {
            GameObject.Find("Jugador_Caballito").GetComponent<Personaje>().muerto = true;
            Destroy(this);
        }

        if (puntos == puntosActual && recuVidas == true)
        {
            vidas++;
            Debug.Log("+1 vida");
            recuVidas = false;
        }
        else if (vidas == 10)
        {
            vidas += 0;
        }

        if (puntos == puntosActual + 1)
        {
            recuVidas = true;
            puntosActual = puntos + 49;
        }
    }
    IEnumerator Polvos()
    {
        int randomTime = Random.Range(20, 75);
        yield return new WaitForSeconds(randomTime);
        GameObject.Instantiate(prefabPolvos);
        yield return StartCoroutine(Polvos());
        if (vidas == 0)
        {
            StopCoroutine(Polvos());
        }
    }
}