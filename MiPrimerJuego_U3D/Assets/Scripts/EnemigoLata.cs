﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoLata : MonoBehaviour
{
    GameObject jugador;
    public float velocidad = 5; 
    private float lataAnchura = 1.6f;
    private float lataAltura = 2.4f;
    private float caballitoAnchura = 1.6f;
    private float caballitoAltura = 2.4f;
    void Start()
    {
        float posInicioX = Random.Range(-11.5f, 11.5f);
        this.transform.position = new Vector3(posInicioX, 10, 0);
        jugador = GameObject.Find("Jugador_Caballito");
    }
    // Update is called once per frame
    
    void Update()
    { 
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;
        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;
        if (this.GetComponent<Transform>().position.y < -2) 
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -2, 0);    
            Debug.Log("Lata Fondo: Quita vida!");
            GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().vidas -= 1;
            Destroy(this.gameObject);
        }
        if ((transform.position.x + lataAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
            transform.position.x + lataAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
            transform.position.y + lataAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
            transform.position.y + lataAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
            (transform.position.x - lataAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
            transform.position.x - lataAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
            transform.position.y + lataAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
            transform.position.y + lataAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
            (transform.position.x - lataAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
            transform.position.x - lataAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
            transform.position.y - lataAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
            transform.position.y - lataAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
            (transform.position.x + lataAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
            transform.position.x + lataAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
            transform.position.y - lataAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
            transform.position.y - lataAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2))
            {
                Debug.Log("Lata: Suma puntos!");
                GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos += +1;
                Destroy(this.gameObject);
        }
        if (GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos >= 10)
        {
            velocidad = 6;
            if (GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos >= 20)
            {
                velocidad = 7;
                if (GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos >= 30)
                {
                    velocidad = 8;
                    if (GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos >= 40)
                    {
                        velocidad = 10;
                        if (GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().puntos >= 50)
                        {
                            velocidad = 12;
                        }
                    }
                }
            }
        }
    }
    void OnDestroy() 
    {
        GameObject.Find("Controlador-Juego").GetComponent<ControladorJuego>().creacion = true;
    }
}
