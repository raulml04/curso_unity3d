﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public float velocidad = 18;    //18 es el valor x defecto
    public bool muerto = false;
    public bool bonus = false;
    public float tiempoEfectos = 15f;
    // Update is called once per frame
    void Update()
    {
        // Cuando se pulsa <--
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Creación de la rotación
            transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            // Creamos el vector de movimiento, a partir del cálculo que
            // influye la velocidad (40), el vector hacia la izquierda (-1, 0, 0): (-40, 0, 0)
            //Para que se mueva -40 unid. por segundo en vez de por cada frame,
            // multiplicamos por el incremento del tiempo (aprox. 0.02 seg, 50FPS) (0.8, 0, 0)
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
                            //  unidad/ seg x  1 x seg/frame = unidades/frame
            // Una vez que se ha calculado, aplicamos el movimiento
            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posición en el eje X es menor que -14 (margen izq) 
            if (this.GetComponent<Transform>().position.x < -12)
            {
                // entnces recolocamos en el margen izq
                this.GetComponent<Transform>().position = new Vector3(-12, 0, 0);
                Debug.Log("Choque a la izquierda");
            }
        }
        // Cuando se pulsa -->
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
            // Se vuelve a poner vector3.left en vez de right para que no se produzca un bug
            Vector3 vectorMov = velocidad * Vector3.left * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);

            // Si la posición en el eje X es mayor que 13 (margen der) 
            if (this.GetComponent<Transform>().position.x > 12)
            {
                // entnces recolocamos en el margen der
                this.GetComponent<Transform>().position = new Vector3(12, 0, 0);
                Debug.Log("Choque a la derecha");
            }

        }
        if (muerto == true)
        {
            Destroy(this.gameObject);
        }
        if (bonus == true)
        {
            velocidad = 35;
            tiempoEfectos -= Time.deltaTime;
            // o también se puede: tiempoEfectos = tiempoEfectos - Time.deltaTime
            if (tiempoEfectos <= 0)
            {
                bonus = false;
                velocidad = 18;
                tiempoEfectos = 15f;
                Debug.Log("Efectos drogas acabado");
            }
        }
    }
}