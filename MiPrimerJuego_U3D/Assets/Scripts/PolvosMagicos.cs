﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolvosMagicos : MonoBehaviour
{
    GameObject jugador;
    public float velocidad = 3;
    private float polvosAnchura = 1.6f;
    private float polvosAltura = 2.4f;
    private float caballitoAnchura = 1.6f;
    private float caballitoAltura = 2.4f;
    void Start()
    {
        float posInicioX = Random.Range(-11.5f, 11.5f);
        this.transform.position = new Vector3(posInicioX, 10, 0);
        jugador = GameObject.Find("Jugador_Caballito");
    }
    // Update is called once per frame

    void Update()
    {
        Vector3 movAbajo = velocidad * new Vector3(0, -1, 0) * Time.deltaTime;
        this.GetComponent<Transform>().position = this.GetComponent<Transform>().position + movAbajo;
        if (this.GetComponent<Transform>().position.y < -2)
        {
            this.GetComponent<Transform>().position = new Vector3(this.transform.position.x, -2, 0);
            Debug.Log("Polvos Mágicos Fondo");
            Destroy(this.gameObject);
        }
        if ((transform.position.x + polvosAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
        transform.position.x + polvosAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
        transform.position.y + polvosAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
        transform.position.y + polvosAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
        (transform.position.x - polvosAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
        transform.position.x - polvosAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
        transform.position.y + polvosAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
        transform.position.y + polvosAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
        (transform.position.x - polvosAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
        transform.position.x - polvosAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
        transform.position.y - polvosAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
        transform.position.y - polvosAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2) ||
        (transform.position.x + polvosAnchura / 2 >= jugador.transform.position.x - caballitoAnchura / 2 &&
        transform.position.x + polvosAnchura / 2 <= jugador.transform.position.x + caballitoAnchura / 2 &&
        transform.position.y - polvosAltura / 2 >= jugador.transform.position.y - caballitoAltura / 2 &&
        transform.position.y - polvosAltura / 2 <= jugador.transform.position.y + caballitoAltura / 2))
        {
            Debug.Log("Polvos Mágicos Cogidos");
            GameObject.Find("Jugador_Caballito").GetComponent<Personaje>().bonus = true;
            Destroy(this.gameObject);
        }
    }
}
