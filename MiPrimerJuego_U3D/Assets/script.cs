﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class script : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public float velocidad = 40;
    void Update()
    {
        {
            Vector3 vectorMov = velocidad * Vector3.down * Time.deltaTime;
            this.GetComponent<Transform>().Translate(vectorMov);
            if (this.GetComponent<Transform>().position.y < -2)
            {
                this.GetComponent<Transform>().position = new Vector3(transform.position.x, -2, 0);
                Debug.Log("Lata fondo");
            }
        }
    }
}
