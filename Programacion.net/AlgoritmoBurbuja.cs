using System;
using System.Collections.Generic;

public class OrdenarComoHumano
{

    public static void Main()
    {
        List<int> numeros = new List<int>();
        numeros.Add(4);
        numeros.Add(5);
        numeros.Add(3);
        numeros.Add(1);
        numeros.Add(2);
        List<int> numOrde = new List<int>();

        Console.WriteLine("Lista: ");
        MostrarLista(numeros);
        Console.WriteLine("Menor " + Menor(numeros));
        Ordenar(numeros);
        Console.WriteLine("Lista ordenada: ");
        MostrarLista(numOrde);
    }
    static void Ordenar(List<int> lista)
    {
        int k = 0;
        Console.WriteLine("Lista ordenando: ");
        for (int i = 0; i < lista.Count; i++)
        {
            Console.WriteLine("i esta en la posicion: " + i);
            for (int j = i + 1; j < lista.Count - 1;j++)
            {
                Console.WriteLine("j esta en la posicion: " + j);
                if (lista[j] < lista[i])
                {
                    lista[j] = lista[k];
                    Console.WriteLine("k vale: " + lista[k]);
                }
            }
            Console.Write(lista[k] + ", ");
        }
    }

    static void MostrarLista(List<int> lista)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.Write(lista[i] + ", ");
        }
    }
    public static int Menor(List<int> lista)
    {
        int menor = lista[0];
        for (int i = 0; i < lista.Count; i++)
        {
            if (lista[i] < menor)
            {
                menor = lista[i];
            }
        }
        return menor;
    }
}