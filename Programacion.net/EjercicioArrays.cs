using System;

public class EjercicioArrays 
{
	public static void Main() 
	{
		EjercicioAtaques();
	}
	public static void EjercicioAtaques() 
	{
		float [] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool [] enemigosCercanos = {false, true, true, false, true, true};
		/*
		float totalAtaques;
		
		totalAtaques = CalcAtaqueTotalCercanos(ataques, enemigosCercanos);
		Console.WriteLine("Total = " + totalAtaques);
		
		bool [] enemigosCercanos2 = {true, false, true, false, false, true};
		float totalAtaques2 = CalcAtaqueTotalCercanos(ataques, enemigosCercanos2);
		Console.WriteLine("Total = " + totalAtaques2);
		
		// Eejercicio 2)
		string [] ataquesStr = {"3.2", "1.7", "2.4", "5.0", "7.1", "4.8"};
		float totalAtaquesNoCerc = CalcAtaqueTotalNoCercanos(ataquesStr, enemigosCercanos);
		
		// Eejercicio 3)
		float totalAtaques3 = CalcAtaqueTotalCercanos3(ataques, 3f);
		Console.WriteLine("Total de la suma de los >3 = " + totalAtaques3);
		
		// Ejercicio 4)
		float totalMaximo = CalcAtaqueTotal(ataques);
		Console.WriteLine("Ataque máximo = " + totalMaximo);
		*/
		
		// Ejercicio 5)
		float totalAtqMedia = MediaAtaques(ataques);
		Console.WriteLine("Ataque media = " + totalAtqMedia);
		
		// Ejercicio 6)
		
		float totalMin = CalcMin(ataques, enemigosCercanos);
		Console.WriteLine("Ataque mínimo cercano = " + totalMin);
		
		/*// Ejercicio 7)
		float totalDoble = DobleArray(ataques);
		Console.WriteLine("Array doble = " + totalDoble);*/
		
	}
	public static float CalcAtaqueTotalCercanos(float[] ataques, bool[] enemigosCercanos)
	{
		float total;
		
		total = 0; // Inicializar variables!!
		
		// Para cuando los ataques sumen sean verdaderos
		for (int i = 0; i < ataques.Length; i++)
		{	
			Console.WriteLine("Valor de " + i + " es " + ataques[i]);
			if (enemigosCercanos[i])
			{
				total = total + ataques[i];
			}
		}
		return total;
	}
	// Ejericio 2) Generar un array con textos con la info de los NO cercanos
	// El resultado es un array 
	
	public static float CalcAtaqueTotalNoCercanos(string[] ataquesStr, bool[] enemigosCercanos)
	{
		float total;
		
		total = 0;
		
		for (int i = 0; i < ataquesStr.Length; i++)
		{	
			if (enemigosCercanos[i] == false)
			{
				Console.WriteLine("Valor No cercano de " + i + " es = " + ataquesStr[i]);
			}
		}
		return total;
	}
	
	// Ejericio 3) 	Crear una función que calcule el ataque total de los que tengan un ataque > 3
	// 				este tope debe pasarse por parámetros
	
	public static float CalcAtaqueTotalCercanos3(float[] ataques, float tope)
	{
		float total;
		total = 0;
		
		for (int i = 0; i < ataques.Length; i++)
		{	
			if (ataques [i] > tope)
			{
				total = total + ataques[i];
			}
		}
		return total;
	}
	
	
	// Eejercicio 4) Otra que devuelva el ataque máximo del array 
	
	public static float CalcAtaqueTotal(float[] ataques)
	{
		float valMax = ataques[0];
		
		for (int i = 0; i < ataques.Length; i++)
		{	
			if (ataques [i] > valMax)
			{
				valMax = ataques [i];
			}
		}
		return valMax;
	}
	// Ejercicio 5) Crea una función que calcule la media de todos los ataques
	
	public static float MediaAtaques(float [] ataques)
	{
		float total;
		
		total = 0; // Inicializar variables!!
		
		// Para cuando los ataques sumen sean verdaderos
		for (int i = 0; i < ataques.Length; i++)
		{	
			total = (total + ataques[i]);
		}
		float total2 = total/ataques.Length;
		return total2;
		//o return total/ataques.Length; no hace falta crear nuevo float
	}
	
	// Ejercicio 6) Crea una función que calcule el mínimo de los enemigos CERCANOS
	public static float CalcMin(float[] ataques, bool[] enemigosCercanos)
	{
		float valMin = ataques[0];
		
		for (int i = 0; i < ataques.Length; i++)
		{	
			if (enemigosCercanos[i] == true)
			{
				Console.WriteLine("Valor cercano de " + i + " es = " + ataques[i]);
				
				if (ataques [i] < valMin)
				{
					valMin = ataques [i];
				}
			}
		}
		return valMin;
	}
	
	// Ejercicio 7: Crear una función que devuelva un array 
	// que contenga el doble de los ataques:  { 1.6f, 0.85f, ....}
	/*public static float DobleArray(float[] ataques)
	{
		
	}*/
}
