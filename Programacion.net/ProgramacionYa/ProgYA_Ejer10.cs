using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaFor1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejer1();
            // Ejer2();
            // Ejer3();
            // Ejer4();
            // Ejer5();
            // Ejer6();
            // Ejer7();
            Ejer8();
        }

        // Confeccionar un programa que lea n pares de datos, cada par de datos corresponde
        // a la medida de la base y la altura de un tri�ngulo. El programa deber� informar:
        // a) De cada tri�ngulo la medida de su base, su altura y su superficie.
        // b) La cantidad de tri�ngulos cuya superficie es mayor a 12.
        static void Ejer1()
        {
            int baseT, alturaT, superficie, n;
            int suma = 0;
            string linea;
            Console.Write("Numero de triangulos: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            for (int x=1; x <= n; x++)
            {
                Console.Write("Ingrese un valor: ");
                linea = Console.ReadLine();
                baseT = int.Parse(linea);
                Console.Write("Ingrese otro valor: ");
                linea = Console.ReadLine();
                alturaT = int.Parse(linea);
                superficie = baseT * alturaT / 2;
                Console.Write("La superficie del triangulo es de: " + superficie + " ");
                if (superficie > 12)
                {
                    suma = suma + 1;
                }
            }
            Console.Write(" Hay " + suma + " triangulos con una superficie mayor que 12");
        }

        // Desarrollar un programa que solicite la carga de 10 n�meros e imprima la suma 
        // de los �ltimos 5 valores ingresados.
        static void Ejer2()
        {
            int valor;
            int suma = 0;
            int n = 10;
            int m = 5;
            string linea;
            for(int x = 1;x <= n; x++)
            {
                Console.Write("Ingrese un valor: ");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                if (x > n - m)
                {
                    suma = suma + valor;
                }
            }
            Console.Write("El resultado de la suma es: " + suma);
        }

        // Desarrollar un programa que muestre la tabla de multiplicar del 5 (del 5 al 50)
        static void Ejer3()
        {
            int producto;
            int tablaMult = 5;
            int hasta = 10;
            for (int x = 1; x <= hasta; x++)
            {
                producto = tablaMult * x;
                Console.Write(producto + " ");
            }
        }

        // Confeccionar un programa que permita ingresar un valor del 1 al 10 y nos muestre 
        // la tabla de multiplicar del mismo (los primeros 12 t�rminos)
        // Ejemplo: Si ingreso 3 deber� aparecer en pantalla los valores 3, 6, 9, hasta el 36.
        static void Ejer4()
        {
            int producto, tablaMult;
            int hasta = 12;
            string linea;
            Console.Write("Ingrese un valor: ");
            linea = Console.ReadLine();
            tablaMult = int.Parse(linea);
            if (tablaMult > 0 && tablaMult <= 10)
            {
                for (int x = 1; x <= hasta; x++)
                {
                    producto = tablaMult * x;
                    Console.Write(producto + " ");
                }
            }
            else
            {
                Console.Write("Valor fuera del parametro de datos");
            }
        }

        // Realizar un programa que lea los lados de n tri�ngulos, e informar:
        // a) De cada uno de ellos, qu� tipo de tri�ngulo es: equil�tero(tres lados iguales), 
        // is�sceles(dos lados iguales), o escaleno(ning�n lado igual)
        // b) Cantidad de tri�ngulos de cada tipo.
        // c) Tipo de tri�ngulo que posee menor cantidad.
        static void Ejer5()
        {
            int n, lado1, lado2, lado3;
            int equilatero = 0;
            int isosceles = 0;
            int escaleno = 0;
            string linea;
            Console.Write("Inserte el numero de triangulos: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            for (int x = 1; x <= n; x++)
            {
                Console.Write("Inserte el valor del primer lado: ");
                linea = Console.ReadLine();
                lado1 = int.Parse(linea);
                Console.Write("Inserte el valor del segundo lado: ");
                linea = Console.ReadLine();
                lado2 = int.Parse(linea);
                Console.Write("Inserte el valor del tercer lado: ");
                linea = Console.ReadLine();
                lado3 = int.Parse(linea);
                if (lado1 == lado2 && lado1 == lado3)
                {
                    equilatero = equilatero + 1;
                    Console.Write("El triangulo es equilatero");
                }
                else
                {
                    if(lado1 == lado2 || lado1 == lado3 || lado2 == lado3)
                    {
                        isosceles = isosceles + 1;
                        Console.Write("El triangulo es isosceles");
                    }
                    else
                    {
                        escaleno = escaleno + 1;
                        Console.Write("El triangulo es escaleno");
                    }
                }
            }
            Console.Write("Hay " + equilatero + " triangulos equilateros, " + isosceles + " isosceles y " + escaleno + " escalenos");
            if(equilatero == isosceles && equilatero == escaleno)
            {
                Console.Write("Hay la misma cantidad de triangulos de cada tipo");
            }
            else
            {
                if (equilatero < isosceles && equilatero < escaleno)
                {
                    Console.Write("El menor numero de triangulos que hay son los equilateros");
                }
                else
                {
                    if (isosceles < equilatero && isosceles < escaleno)
                    {
                        Console.Write("El menor numero de triangulos que hay son los isosceles");
                    }
                    else
                    {
                        Console.Write("El menor numero de triangulos que hay son los escalenos");
                    }
                }
            }
        }
        
        // Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
        // Informar cu�ntos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante.
        // Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.
        static void Ejer6()
        {
            int x, y, n;
            int primerCuad = 0;
            int segundoCuad = 0;
            int tercerCuad = 0;
            int cuartoCuad = 0;
            string linea;
            Console.Write("Cuantas coordenadas hay? ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            for(int i = 1; i <= n; i++)
            {
                Console.Write("Ingrese un valor para la coordenada X: ");
                linea = Console.ReadLine();
                x = int.Parse(linea);
                Console.Write("Ingrese un valor para y: ");
                linea = Console.ReadLine();
                y = int.Parse(linea);
                if (x > 0 && y > 0)
                {
                    primerCuad = primerCuad + 1;
                }
                else
                {
                    if(x < 0 && y > 0)
                    {
                        segundoCuad = segundoCuad + 1;
                    }
                    else
                    {
                        if (x < 0 && y < 0)
                        {
                            tercerCuad = tercerCuad + 1;
                        }
                        else
                        {
                            cuartoCuad = cuartoCuad + 1;
                        }
                    }
                }
            }
            Console.Write("Hay " + primerCuad + " coordenadas en el primer cuadrante");
            Console.Write("Hay " + segundoCuad + " coordenadas en el segundo cuadrante");
            Console.Write("Hay " + tercerCuad + " coordenadas en el tercer cuadrante");
            Console.Write("Hay " + cuartoCuad + " coordenadas en el cuarto cuadrante");
        }

        // Se realiza la carga de 10 valores enteros por teclado. Se desea conocer:
        // a) La cantidad de valores ingresados negativos.
        // b) La cantidad de valores ingresados positivos.
        // c) La cantidad de m�ltiplos de 15.
        // d) El valor acumulado de los n�meros ingresados que son pares.
        static void Ejer7()
        {
            int valor;
            int n = 10;
            int negativo = 0;
            int positivo = 0;
            int multiplo = 0;
            int pares = 0;
            string linea;
            for (int x = 1; x <= n; x++)
            {
                Console.Write("Ingrese un valor: ");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                if (valor < 0)
                {
                    negativo = negativo + 1;
                }
                else
                {
                    positivo = positivo + 1;
                }
                if (valor%15 == 0)
                {
                    multiplo = multiplo + 1;
                }
                if (valor%2 == 0)
                {
                    pares = pares + valor;
                }
            }
            Console.Write("Hay " + negativo + " numeros negativos");
            Console.Write("Hay " + positivo + " numeros positivos");
            Console.Write("Hay " + multiplo + " numeros que son multiplo de 15");
            Console.Write("La suma de los numeros pares es: " + pares);
        }

        // Se cuenta con la siguiente informaci�n:
        // Las edades de 50 estudiantes del turno ma�ana.
        // Las edades de 60 estudiantes del turno tarde.
        // Las edades de 110 estudiantes del turno noche.
        // Las edades de cada estudiante deben ingresarse por teclado.
        // a) Obtener el promedio de las edades de cada turno(tres promedios)
        // b) Imprimir dichos promedios(promedio de cada turno)
        // c) Mostrar por pantalla un mensaje que indique cual de los tres turnos 
        // tiene un promedio de edades mayor.
        static void Ejer8()
        {
            int edad;
            int edad1 = 5;
            int edad2 = 6;
            int edad3 = 11;
            int suma1 = 0;
            int suma2 = 0;
            int suma3 = 0;
            int promedio1 = 0;
            int promedio2;
            int promedio3;
            string linea;
            for(int x = 1; x <= edad1; x++)
            {
                Console.Write("Ingrese la edad del estudiante de ma�ana: ");
                linea = Console.ReadLine();
                edad = int.Parse(linea);
                suma1 = suma1 + edad;
            }
            for (int x = 1; x <= edad2; x++)
            {
                Console.Write("Ingrese la edad del estudiante de tarde: ");
                linea = Console.ReadLine();
                edad = int.Parse(linea);
                suma2 = suma2 + edad;
            }
            for (int x = 1; x <= edad3; x++)
            {
                Console.Write("Ingrese la edad del estudiante de noche: ");
                linea = Console.ReadLine();
                edad = int.Parse(linea);
                suma3 = suma3 + edad;
            }
            promedio1 = suma1 / edad1;
            promedio2 = suma2 / edad2;
            promedio3 = suma3 / edad3;
            Console.Write("La media de edad del turno de ma�ana es: " + promedio1);
            Console.Write("La media de edad del turno de tarde es: " + promedio2);
            Console.Write("La media de edad del turno de noche es: " + promedio3);
            if (promedio1 == promedio2 && promedio1 == promedio3)
            {
                Console.Write("La media de edad es la misma en los tres turnos");
            }
            else
            {
                if (promedio1 > promedio2 && promedio1 > promedio3)
                {
                    Console.Write("La media del turno de ma�ana es mayor");
                }
                else
                {
                    if(promedio2 > promedio1 && promedio2 > promedio3)
                    {
                        Console.Write("La media del turno de tarde es mayor");
                    }
                    else
                    {
                        Console.Write("La media del turno de noche es mayor");
                    }
                }
            }
        }
    }
}