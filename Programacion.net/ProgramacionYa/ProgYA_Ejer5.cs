﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejer1();
            //Ejer2();
            //Ejer3();
            Ejer4();
        }

        // Realizar la carga del lado de un cuadrado, mostrar por pantalla el perímetro del mismo 
        // (El perímetro de un cuadrado se calcula multiplicando el valor del lado por cuatro)
        static void Ejer1()
        {
            int num1, perimetro;
            string linea;
            Console.Write("Ingrese valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            perimetro = num1 * 4;
            Console.WriteLine("El perimetro del cuadrado es: " + num1 + " x 4 = " + perimetro);
        }

        // Escribir un programa en el cual se ingresen cuatro números, calcular e informar
        // la suma de los dos primeros y el producto del tercero y el cuarto.
        static void Ejer2()
        {
            int num1, num2, num3, num4, suma, producto;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            Console.Write("Ingrese cuarto valor:");
            linea = Console.ReadLine();
            num4 = int.Parse(linea);
            suma = num1 + num2;
            producto = num3 * num4;
            Console.WriteLine("La suma de: " + num1 + " + " + num2 + " = " + suma);
            Console.WriteLine("El producto de: " + num3 + " x " + num4 + " = " + producto);
        }

        // Realizar un programa que lea cuatro valores numéricos e informar su suma y promedio.
        static void Ejer3()
        {
            int num1, num2, num3, num4, suma, media;
            string linea;
            Console.Write("Ingrese primer valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo valor:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese tercer valor:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            Console.Write("Ingrese cuarto valor:");
            linea = Console.ReadLine();
            num4 = int.Parse(linea);
            suma = num1 + num2 + num3 + num4;
            media = suma / 4;
            Console.WriteLine("La suma de: " + num1 + " + " + num2 + " + " + num3 + " + " + num4 + " = " + suma);
            Console.WriteLine("La media es = " + media);

        }

        // Se debe desarrollar un programa que pida el ingreso del precio de un artículo y la cantidad 
        // que lleva el cliente. Mostrar lo que debe abonar el comprador.
        static void Ejer4()
        {
            int precio, cantidad, total;
            string linea;
            Console.Write("Valor del producto: ");
            linea = Console.ReadLine();
            precio = int.Parse(linea);
            Console.Write("Cantidad de producto: ");
            linea = Console.ReadLine();
            cantidad = int.Parse(linea);
            total = precio * cantidad;
            Console.WriteLine("El precio total es = " + total);
        }
    }
}
