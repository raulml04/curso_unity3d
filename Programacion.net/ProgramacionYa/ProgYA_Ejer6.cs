using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalSimple1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejer1();
            //Ejer2();
            Ejer3();
        }

        // Realizar un programa que lea por teclado dos n�meros, si el primero es mayor al segundo informar 
        // su suma y diferencia, en caso contrario informar el producto y la divisi�n del primero respecto al segundo.
        static void Ejer1()
        {
            int num1, num2, suma, resta, producto, division;
            string linea;
            Console.Write("Ingrese el valor del primer numero: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese el valor del segundo numero: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            if (num1 > num2)
            {
                suma = num1 + num2;
                resta = num1 - num2;
                Console.WriteLine("La suma de los numeros es: " + num1 + " + " + num2 + " = " + suma);
                Console.WriteLine("La resta de los numeros es: " + num1 + " - " + num2 + " = " + resta);
            }
            else
            {
                producto = num1 * num2;
                division = num1 / num2;
                Console.WriteLine("El producto de los numeros es: " + num1 + " x " + num2 + " = " + producto);
                Console.WriteLine("La division de los numeros es: " + num1 + " / " + num2 + " = " + division);
            }
        }

        // Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado".
        static void Ejer2()
        {
            int nota1, nota2, nota3, suma, media;
            string linea;
            Console.Write("Ingrese la primera nota: ");
            linea = Console.ReadLine();
            nota1 = int.Parse(linea);
            Console.Write("Ingrese la segunda nota: ");
            linea = Console.ReadLine();
            nota2 = int.Parse(linea);
            Console.Write("Ingrese la tercera nota: ");
            linea = Console.ReadLine();
            nota3 = int.Parse(linea);
            suma = nota1 + nota2 + nota3;
            media = suma / 3;
            Console.WriteLine("La media del alumno es: " + "( " + nota1 + " + " + nota2 + " + " + nota3 + " ) / 3 = " + media);
            if (media > 7)
            {
                Console.Write("Promocionado");
            }
            else
            {
                Console.Write("No promociona");
            }
        }

        // Se ingresa por teclado un n�mero positivo de uno o dos d�gitos (1..99) 
        // mostrar un mensaje indicando si el n�mero tiene uno o dos d�gitos.
        // (Tener en cuenta que condici�n debe cumplirse para tener dos d�gitos, un n�mero entero)
        static void Ejer3()
        {
            int num;
            string linea;
            Console.Write("Ingrese el valor del numero: ");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if (num < 10)
            {
                Console.Write("Tiene un digito");
            }
            else
            {
                if (num > 99)
                {
                    Console.Write("Numero demasiado grande");
                }
                else
                {
                    Console.Write("Tiene dos digitos");
                }
            }
            
        }
    }
}