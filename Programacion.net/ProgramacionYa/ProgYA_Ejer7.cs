using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraCondicionalAnidada1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejer1();
            //Ejer2();
            //Ejer3();
            Ejer4();
        }

        // Se cargan por teclado tres n�meros distintos. Mostrar por pantalla el mayor de ellos
        static void Ejer1()
        {
            int num1, num2, num3;
            string linea;
            Console.Write("Ingrese primer numero:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese segundo numero:");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese tercer numero:");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1 > num2)
            {
                if (num1 > num3)
                {
                    Console.WriteLine(num1 + " es el mayor.");
                }
                else
                {
                    Console.WriteLine(num3 + " es el mayor.");
                }
            }
            else
            {
                if (num2 > num3)
                {
                    Console.WriteLine(num2 + " es el mayor.");
                }
                else
                {
                    Console.WriteLine(num3 + " es el mayor.");
                }
            }
        }

        // Se ingresa por teclado un valor entero, mostrar una leyenda que indique si el n�mero es 
        // positivo, nulo o negativo.
        static void Ejer2()
        {
            int num;
            string linea;
            Console.Write("Ingrese primer numero:");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if (num > 0)
            {
                Console.WriteLine(num + " es positivo");
            }
            else
            {
                if(num == 0)
                {
                    Console.WriteLine(num + " es nulo");
                }
                else
                {
                    Console.WriteLine(num + " es negativo");
                }
            }
        }

        // Confeccionar un programa que permita cargar un n�mero entero positivo 
        // de hasta tres cifras y muestre un mensaje indicando si tiene 1, 2, o 3 cifras. 
        // Mostrar un mensaje de error si el n�mero de cifras es mayor.
        static void Ejer3()
        {
            int num;
            string linea;
            Console.Write("Ingrese el valor del numero: ");
            linea = Console.ReadLine();
            num = int.Parse(linea);
            if(num >= 0)
            {
                if (num < 10)
                {

                    Console.Write(num + " tiene un digito");
                }
                else
                {
                    if (num < 100)
                    {
                        Console.Write(num + " tiene dos digitos");
                    }
                    else
                    {
                        if (num < 1000)
                        {
                            Console.Write(num + " tiene tres digitos");
                        }
                        else
                        {
                            Console.Write("Error, numero demasiado grande");
                        }
                    }
                }
            }
            else
            {
                Console.Write("Error, numero negativo");
            }
        }

        // Un postulante a un empleo, realiza un test de capacitaci�n, se obtuvo la siguiente informaci�n:
        // cantidad total de preguntas que se le realizaron y la cantidad de preguntas que contest� correctamente.
        // Se pide confeccionar un programa que ingrese los dos datos por teclado e informe el nivel del mismo 
        // seg�n el porcentaje de respuestas correctas que ha obtenido, y sabiendo que:
        // Nivel m�ximo:	Porcentaje>=90%.
        // Nivel medio:	Porcentaje>=75% y<90%.
        // Nivel regular:	Porcentaje>=50% y<75%.
        // Fuera de nivel:	Porcentaje<50%.
        static void Ejer4()
        {
            int numPreg, pregBien, porcentaje; 
            string linea;
            Console.Write("Numero de preguntas realizadas: ");
            linea = Console.ReadLine();
            numPreg = int.Parse(linea);
            Console.Write("Numero de preguntas acertadas: ");
            linea = Console.ReadLine();
            pregBien = int.Parse(linea);
            porcentaje = (100 * pregBien) / numPreg;
            Console.Write("Porcentaje de aciertos: " + porcentaje);
            if (porcentaje >= 90)
            {
                Console.WriteLine(" Nivel maximo");
            }
            else
            {
                if(porcentaje >= 75)
                {
                    Console.WriteLine(" Nivel medio");
                }
                else
                {
                    if (porcentaje >= 75)
                    {
                        Console.WriteLine(" Nivel medio");
                    }
                    else
                    {
                        if (porcentaje >= 50)
                        {
                            Console.WriteLine(" Nivel regular");
                        }
                        else
                        {
                            Console.WriteLine(" Fuera de nivel");
                        }
                    }
                }
            }
        }
    }
}