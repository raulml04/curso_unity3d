using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondicionCompuesta1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejer1();
            // Ejer2();
            // Ejer3();
            // Ejer4();
            // Ejer5();
            // Ejer6();
            Ejer7();
        }

        // Realizar un programa que pida cargar una fecha cualquiera, luego verificar si dicha fecha corresponde a Navidad.
        static void Ejer1()
        {
            int dia, mes;
            string linea;
            Console.Write("Ingrese el dia: ");
            linea = Console.ReadLine();
            dia = int.Parse(linea);
            Console.Write("Ingrese el numero del mes: ");
            linea = Console.ReadLine();
            mes = int.Parse(linea);
            if (dia == 25 && mes == 12)
            {
                Console.Write("Feliz Navidad");
            }
            else
            {
                if (dia <= 31 && mes == 1)
                {
                    Console.Write("Es " + dia + " de enero");
                }
                if (dia <= 29 && mes == 2)
                {
                    Console.Write("Es " + dia + " de febrero");
                }
                if (dia <= 31 && mes == 3)
                {
                    Console.Write("Es " + dia + " de marzo");
                }
                if (dia <= 30 && mes == 4)
                {
                    Console.Write("Es " + dia + " de abril");
                }
                if (dia <= 31 && mes == 5)
                {
                    Console.Write("Es " + dia + " de mayo");
                }
                if (dia <= 30 && mes == 6)
                {
                    Console.Write("Es " + dia + " de junio");
                }
                if (dia <= 31 && mes == 7)
                {
                    Console.Write("Es " + dia + " de julio");
                }
                if (dia <= 31 && mes == 8)
                {
                    Console.Write("Es " + dia + " de agosto");
                }
                if (dia <= 30 && mes == 9)
                {
                    Console.Write("Es " + dia + " de septiembre");
                }
                if (dia <= 31 && mes == 10)
                {
                    Console.Write("Es " + dia + " de octubre");
                }
                if (dia <= 30 && mes == 11)
                {
                    Console.Write("Es " + dia + " de noviembre");
                }
                if (dia <= 31 && mes == 12)
                {
                    Console.Write("Es " + dia + " de diciembre");
                }
            }
        }

        // Se ingresan tres valores por teclado, si todos son iguales se imprime la suma del primero con el segundo 
        // y a este resultado se lo multiplica por el tercero.
        static void Ejer2()
        {
            int num1, num2, num3, operacion;
            string linea;
            Console.Write("Ingrese un valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese un segundo valor: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese un tercer valor: ");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            operacion = (num1 + num2) * num3;
            if (num1 == num2 && num1 == num3)
            {
                Console.Write("( " + num1 + " + " + num2 + " ) x " + num3 + " = " + operacion);
            }
        }

        // Se ingresan por teclado tres n�meros, si todos los valores ingresados son menores a 10, 
        // imprimir en pantalla la leyenda "Todos los n�meros son menores a diez".
        static void Ejer3()
        {
            int num1, num2, num3;
            string linea;
            Console.Write("Ingrese un valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese un segundo valor: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese un tercer valor: ");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1 < 10 && num2 < 10 && num3 < 10)
            {
                Console.Write("Todos los numeros son menores a diez");
            }
        }

        // Se ingresan por teclado tres n�meros, si al menos uno de los valores ingresados es menor a 10, 
        // imprimir en pantalla la leyenda "Alguno de los n�meros es menor a diez".
        static void Ejer4()
        {
            int num1, num2, num3;
            string linea;
            Console.Write("Ingrese un valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese un segundo valor: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese un tercer valor: ");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1 < 10 || num2 < 10 || num3 < 10)
            {
                Console.Write("Alguno de los numeros es menor a diez");
            }
        }

        // Escribir un programa que pida ingresar la coordenada de un punto en el plano, 
        // es decir dos valores enteros x e y (distintos a cero).
        // Posteriormente imprimir en pantalla en que cuadrante se ubica dicho punto. 
        // (1� Cuadrante si x > 0 Y y > 0 , 2� Cuadrante: x< 0 Y y> 0, etc.)
        static void Ejer5()
        {
            int x, y;
            string linea;
            Console.Write("Ingrese un valor para X: ");
            linea = Console.ReadLine();
            x = int.Parse(linea);
            Console.Write("Ingrese un valor para Y: ");
            linea = Console.ReadLine();
            y = int.Parse(linea);
            if (x > 0 && y > 0)
            {
                Console.Write("La coordenada X: " + x + " Y: " + y + " esta en el primer cuadrante");
            }
            else
            {
                if (x < 0 && y > 0)
                {
                    Console.Write("La coordenada X: " + x + " Y: " + y + " esta en el segundo cuadrante");
                }
                else
                {
                    if (x < 0 && y < 0)
                    {
                        Console.Write("La coordenada X: " + x + " Y: " + y + " esta en el tercer cuadrante");
                    }
                    else
                    {
                        Console.Write("La coordenada X: " + x + " Y: " + y + " esta en el cuarto cuadrante");
                    }
                }
            }
            
        }

        // De un operario se conoce su sueldo y los a�os de antig�edad. Se pide confeccionar 
        //un programa que lea los datos de entrada e informe:
        // a) Si el sueldo es inferior a 500 y su antig�edad es igual o superior a 10 a�os, 
        //otorgarle un aumento del 20 %, mostrar el sueldo a pagar.
        // b)Si el sueldo es inferior a 500 pero su antig�edad es menor a 10 a�os, otorgarle un aumento de 5 %.
        // c) Si el sueldo es mayor o igual a 500 mostrar el sueldo en pantalla sin cambios.
        static void Ejer6()
        {
            int sueldo, antiguo, porcentaje, suma;
            string linea;
            Console.Write("Ingrese su sueldo: ");
            linea = Console.ReadLine();
            sueldo = int.Parse(linea);
            Console.Write("Ingrese su antiguedad en la empresa: ");
            linea = Console.ReadLine();
            antiguo = int.Parse(linea);
            if (sueldo < 500 && antiguo >= 10)
            {
                porcentaje = sueldo * 20 / 100;
                suma = porcentaje + sueldo;
                Console.Write("Se le aumenta un 20% su sueldo: " + porcentaje + ", su sueldo sera: " + suma);
            }
            else
            {
                if (sueldo < 500 && antiguo < 10)
                {
                    porcentaje = sueldo * 5 / 100;
                    suma = porcentaje + sueldo;
                    Console.Write("Se le aumenta un 5% su sueldo: " + porcentaje + ", su sueldo sera: " + suma);
                }
                else
                {
                    Console.Write("Su sueldo no sufre cambios: " + sueldo);
                }
            }
        }

        // Escribir un programa en el cual: dada una lista de tres valores num�ricos distintos se calcule 
        // e informe su rango de variaci�n (debe mostrar el mayor y el menor de ellos)
        static void Ejer7()
        {
            int num1, num2, num3;
            string linea;
            Console.Write("Ingrese un valor: ");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            Console.Write("Ingrese un segundo valor: ");
            linea = Console.ReadLine();
            num2 = int.Parse(linea);
            Console.Write("Ingrese un tercer valor: ");
            linea = Console.ReadLine();
            num3 = int.Parse(linea);
            if (num1 > num2)
            {
                if (num1 > num3)
                {
                    Console.Write(num1 + " es el mayor ");
                    if (num2 < num3)
                    {
                        Console.Write(num2 + " es el menor ");
                    }
                    else
                    {
                        Console.Write(num3 + " es el menor ");
                    }
                }
                else
                {
                    Console.Write(num3 + " es el mayor ");
                    Console.Write(num2 + " es el menor ");
                }
            }
            else
            {
                if (num2 < num3)
                {
                    Console.Write(num3 + " es el mayor ");
                    Console.Write(num1 + " es el menor ");
                }
                else
                {
                    Console.Write(num2 + " es el mayor ");
                    if (num1 < num3)
                    {
                        Console.Write(num1 + " es el menor ");
                    }
                    else
                    {
                        Console.Write(num3 + " es el menor ");
                    }
                }
            }
        }
    }
}