using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstructuraRepetitivaWhile1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Ejer1();
            // Ejer2();
            // Ejer3();
            // Ejer4();
            // Ejer5();
            // Ejer6();
            Ejer7();
        }

        // Escribir un programa que solicite ingresar 10 notas de alumnos y nos informe
        // cu�ntos tienen notas mayores o iguales a 7 y cu�ntos menores.
        static void Ejer1()
        {
            int nota;
            int alumno = 1;
            int aprob = 0;
            int susp = 0;
            string linea;
            while (alumno <= 10)
            {
                Console.Write("Ingrese la nota del alumno:");
                linea = Console.ReadLine();
                nota = int.Parse(linea);
                if (nota >= 7)
                {
                    aprob = aprob + 1;
                }
                else
                {
                    susp = susp + 1;
                }
                alumno++;
            }
            Console.Write("Numero de alumnos con 7 o mas: " + aprob);
            Console.Write(" Numero de alumnos con menos de 7: " + susp);
        }

        // Se ingresan un conjunto de n alturas de personas por teclado. 
        // Mostrar la altura promedio de las personas.
        static void Ejer2()
        {
            int n, media;
            int altura = 0;
            int x = 1;
            string linea;
            Console.Write("Cuantas personas hay?: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            while (x <= n)
            {
                Console.Write("Ingrese una altura: ");
                linea = Console.ReadLine();
                altura = altura + int.Parse(linea);
                x++;
            }
            media = altura / n;
            Console.Write("La altura media es: " + media);
        }

        // En una empresa trabajan n empleados cuyos sueldos oscilan entre $100 y $500, 
        // realizar un programa que lea los sueldos que cobra cada empleado e informe 
        // cu�ntos empleados cobran entre $100 y $300 y cu�ntos cobran m�s de $300. 
        // Adem�s el programa deber� informar el importe que gasta la empresa en sueldos al personal.
        static void Ejer3()
        {
            int sueldo, n;
            int x = 1;
            int gastoSueldo = 0;
            int cobra1 = 0;
            int cobra2 = 0;
            string linea;
            Console.Write("Cuantos empleados tiene su empresa?: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            while (x <= n)
            {
                Console.Write("Ingrese el sueldo: ");
                linea = Console.ReadLine();
                sueldo = int.Parse(linea);
                if (sueldo < 100 || sueldo > 500)
                {
                    Console.Write("Sueldo no valido ");
                }
                else
                {
                    if (sueldo >= 100 && sueldo <= 300)
                    {
                        cobra1 = cobra1 + 1;
                    }
                    else
                    {
                        cobra2 = cobra2 + 1;
                    }
                    gastoSueldo = gastoSueldo + sueldo;
                }
                x++;
            }
            Console.Write(" Empleados que cobran entre 100 y 300: " + cobra1);
            Console.Write(" Empleados que cobran mas de 300: " + cobra2);
            Console.Write(" Gasto en sueldos de la empresa: " + gastoSueldo);
        }

        // Realizar un programa que imprima 25 t�rminos de la serie 11 - 22 - 33 - 44, etc. 
        // (No se ingresan valores por teclado)
        static void Ejer4()
        {
            int n = 25;
            int x = 1;
            int variable = 11;
            int resultado = 0;
            while (x <= n)
            {
                resultado = variable * x;
                Console.Write(resultado + "  ");
                x++;
            }
        }

        // Mostrar los m�ltiplos de 8 hasta el valor 500. Debe aparecer en pantalla 8 - 16 - 24, etc
        static void Ejer5()
        {
            int n = 500;
            int x = 1;
            int variable = 8;
            int resultado = 0;
            while (resultado <= n)
            {
                resultado = variable * x;
                Console.Write(resultado + "  ");
                x++;
            }
        }

        // Realizar un programa que permita cargar dos listas de 15 valores cada una. 
        // Informar con un mensaje cual de las dos listas tiene un valor acumulado mayor 
        // (mensajes "Lista 1 mayor", "Lista 2 mayor", "Listas iguales")
        // Tener en cuenta que puede haber dos o m�s estructuras repetitivas en un algoritmo.
        static void Ejer6()
        {
            int numl1, numl2;
            int lista1 = 0;
            int lista2 = 0;
            int n = 15;
            int x = 1;
            int y = 1;
            string linea;
            while (x <= n)
            {
                Console.Write("Ingrese un valor para la primera lista: ");
                linea = Console.ReadLine();
                numl1 = int.Parse(linea);
                lista1 = lista1 + numl1;
                x++;
            }
            while (y <= n)
            {
                Console.Write("Ingrese un valor para la segunda lista: ");
                linea = Console.ReadLine();
                numl2 = int.Parse(linea);
                lista2 = lista2 + numl2;
                y++;
            }
            if (lista1 == lista2)
            {
                Console.Write("Listas iguales: " + lista1 + " = " + lista2);
            }
            else
            {
                if (lista1 > lista2)
                {
                    Console.Write("Lista 1 mayor: " + lista1 + " > " + lista2);
                }
                else
                {
                    Console.Write("Lista 2 mayor: " + lista2 + " > " + lista1);
                }
            }
        }

        // Desarrollar un programa que permita cargar n n�meros enteros y luego nos informe 
        // cu�ntos valores fueron pares y cu�ntos impares.
        // Emplear el operador �%� en la condici�n de la estructura condicional:
	    // if (valor%2==0)         //Si el if da verdadero luego es par.
        static void Ejer7()
        {
            int valor, n;
            int pares = 0;
            int impares = 0;
            int x = 1;
            string linea;
            Console.Write("Ingrese cuantos valores se van a usar: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            while(x <= n)
            {
                Console.Write("Ingrese un valor: ");
                linea = Console.ReadLine();
                valor = int.Parse(linea);
                if (valor%2 == 0)
                {
                    pares = pares + 1;
                }
                else
                {
                    impares = impares + 1;
                }
                x++;
            }
            Console.Write("Hay " + pares + " numeros pares y " + impares + " numeros impares");
        }
    }
}