using System;

// Es una estructura de datos, que puede tener
// cero, uno o muchos elementos DEL MISMO TIPO
// y en principio el nº de elementos es FIJO
public class EjemploArrays 
{
	public static void Main () 
	{
		/*
		ArrayDeDatosPrimmitivos();
		ArrayDeFloats();
		ArrayDeStrings ();
		*/
		float [] ataques = {3.2f, 1.7f, 2.4f, 5.0f, 7.1f, 4.8f};
		bool [] enemigosCercanos = {false, true, true, false, true, true};
		int suma = Int32.Parse(ataques);
		Ejercicio (); 
	}
	public static void ArrayDeDatosPrimmitivos () 
	{
		Console.WriteLine("Array enteros");
		// Declaración <tipo> [] <nombreVariable>
		int[] numerosPares;
		//Iniciar con el nº de elementos
		// new se encarga de reservar la memoria para la 
		// estructura que sea
		numerosPares = new int [4];
		// Para asignar un valor en una posición: 
		// los arrays empiezan por cero 
		numerosPares [0] = 2;
		numerosPares [1] = 4;
		numerosPares [2] = 6;
		numerosPares [3] = 8;
		// el último elemento está en la pos. longitud del array -1
		
		for (int i = 0; i < numerosPares.Length ; i = i + 1)
		{
			Console.WriteLine("Elemento " + i + " = " + numerosPares[i]);
		}
	}
	public static void ArrayDeFloats () 
	{
		float [] fuerzas = new float [5];
		for (int i = 0; i<= fuerzas.Length - 1; i += 1)
		{
			fuerzas[i] = 1.2345f + i;
		}
		Console.WriteLine("¿Qué fuerza quieres ver?");
		string strTecla = Console.ReadKey().Key.ToString();
		string numero;
		// Para extraer el número probamos varias maneras.
		// 1) Usando string.Remove():
		numero = strTecla.Remove(0, strTecla.Length - 1);
		// 2) Usando el string como si fuera un array unidim de char
		numero = strTecla[strTecla.Length - 1].ToString();
		// 3) Usando la típica para estos casos que es Substring
		numero = strTecla.Substring(strTecla.Length - 1, 1);
		
		int pos = Int32.Parse(numero);
		Console.WriteLine("La fuerza núm " + pos + " es de: ");
		Console.WriteLine(fuerzas[pos] + " newtons");
	}
	public static void ArrayDeStrings ()
	{
		Console.WriteLine("¿Cuántos nombres quiere guaardar?");
		string strCantidad = Console.ReadLine();
		int cantidad = Int32.Parse(strCantidad);
		
		string [] nombres;
		nombres = new string [cantidad];
		for (int i = 0; i < cantidad; i++)
		{
			string nuevoNombre = Console.ReadLine();
			nombres [i] = nuevoNombre;
		}
		Console.WriteLine("Listado de nombre: ");
		
		for (int i = 0; i < cantidad; i++)
		{
			Console.WriteLine($" - {i} = {nombres[i]}");
		}
	}
	public static void Ejercicio ()
	{
		
		for (int i = 0; i < ataques.Length; i++)
		{	
			Console.WriteLine("  ");
			
		}
	}
	
}