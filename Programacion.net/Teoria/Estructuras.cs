using System;

struct Jugador
{
    public string nombre;
    public int edad;
    public bool haPagado; // true ha pagado, false es free to play
    
    public void Mostrar()
    {
        Console.WriteLine("Jugador: " + this.nombre 
            + ", edad = " + this.edad 
            + (haPagado ? ", VIP" : ", Free To Play"));
    }
}
struct Enemigo
{
    public string nombre;
    public float ataque;
    
    public void Mostrar()
    {
        Console.WriteLine("Enemigo: " + this.nombre + ", ataque = " + this.ataque);
    }
    public void MostrarConAtaque()
    {
        Console.WriteLine("Enemigo: " + this.nombre + ", ataque = " + this.ataque);
    }
}

public class Class1
{

    static public void Main()
    {
        Jugador jug = new Jugador("Fulano", 30, false);
        jug.Mostrar();
        // MostrarPersona(jug.nombre, jug.edad, jug.haPagado);

        Jugador jug2 = new Jugador("Fulanita", 32, true);
        jug2.Mostrar();

        Enemigo enemigo = new Enemigo("Chtulu", 201);
        enemigo.Mostrar();
        enemigo.MostrarConAtaque("Hechizo");
    }
}