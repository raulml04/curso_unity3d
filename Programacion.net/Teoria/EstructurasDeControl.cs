using System;

public class EstructurasDeControl {
	public static void Main () 
	{
		/*
		EjemploIfSimple();
		EjemploIfComplicado();
		EjercicioIfSumas();
		EjemploIfConsecutivos();
		EjemploBucleWhile();
		EjemploBucleFor();
		*/
		EjemploBucleDoWhile();
	}
	static void EjemploIfSimple()
	{
		// Condicional simple: if (booleano) instrucción;
		if (true) Console.WriteLine("Pues sí");
		if (false) Console.WriteLine("Pues va a ser que no");
		
		// Recibe variable
		bool oSioNo = true;
		if (oSioNo) Console.WriteLine("Pues también sí");
		// O recibimos condicionales
		if (5 == 5) Console.WriteLine("Pues 5 == 5");
		if (4 > 7) Console.WriteLine("Pues esto tampoco se muestra");
	}
	static void EjemploIfComplicado()
	{
		// if complicado:	
		// if(bool) instruccionVerdadera; else instruccionFalso;
		if (4 >=7) Console.WriteLine("4 >= 7"); 
		else Console.WriteLine("4 < 7");
		if ("Hola" != "hola") Console.WriteLine("Son distintos"); 
		else Console.WriteLine("Son =");
	}
	static void EjercicioIfSumas()
	{
		// Ejercicio: Comprueba las 3 combinaciones (A+B, B+C, A+C)
		// y que el problema diga cual es el resultado
		// Consola debe mostrar:
		// 1ro mostrar los valores: A = 20, B = 30, C = 40, resultado 50
		// A+B es igual al resultado
		// A+C es distinto al resultado
		// B+C es distinto al resultado
		string numA = "20", numB = "30", numC = "40";
		int resultado = 60;
		int numAA = Int32.Parse(numA);
		int numBB = Int32.Parse(numB);
		int numCC = Int32.Parse(numC);
		int sumaA = numAA + numBB;
		int sumaB = numAA + numCC;
		int sumaC = numBB + numCC;
		if (sumaA == resultado) Console.WriteLine(numA + "+" + numB + " = " + resultado); else Console.WriteLine("Son distintos"); 
		if (sumaB == resultado) Console.WriteLine(numA + "+" + numC + " = " + resultado); else Console.WriteLine("Son distintos");
		if (sumaC == resultado) Console.WriteLine(numB + "+" + numC + " = " + resultado); else Console.WriteLine("Son distintos");
	}
	static void EjemploIfConsecutivos() 
	{
		Console.WriteLine("Introduzca una opción: ");
		Console.WriteLine("1 - Opción primera ");
		Console.WriteLine("2 - Opción segunda ");
		Console.WriteLine("3 - Opción tercera ");
		Console.WriteLine("(*) - Cualquier otra opción.");
		
		ConsoleKeyInfo opcion = Console.ReadKey();
		ConsoleKey conKey = opcion.Key;
		string caracter = conKey.ToString();
	
		Console.WriteLine(" >> " + caracter);
		// Si el caracter es un 1 del teclado normla o bien
		// si el caracter es un 1 del teclao numérico
		if (caracter == "NumPad1" || caracter == "D1")
			Console.WriteLine("Has elegido la primera");
		else if (caracter == "NumPad2" || caracter == "D2")
			Console.WriteLine("Has elegido la segunda");
		else if (caracter == "NumPad3" || caracter == "D3")
			Console.WriteLine("Has elegido la tercera");
		else if (caracter == "NumPad4" || caracter == "D4")
			Console.WriteLine("Has elegido la cuarta");
		else
			Console.WriteLine("OPCION NO CONTEMPLADA ");
	}
	
	static void EjemploBucleWhile() 
	{
	// El bucle while puede crear cualquier tipo de bucle
	// Puede que nunca se ejecute si el booleano (condición) es false de entrada
	// o puede que sea un bucle infinito, si la condición siempre es true
	// o un bucle normal con una duración determinada
		{
			Console.WriteLine("Antes de bucle");
			
			while(false)
				Console.WriteLine("Instrucción que NO se repite");
		}
		int contador = 0;
		while (contador < 10)
		{
			Console.WriteLine("Contador = " + contador);
			contador = contador + 1;
		}
		bool siContinuar = true;
		while (siContinuar)
		{
			Console.WriteLine("Estamos en bucle. ¿Desea salir?");
			string tecla = Console.ReadKey().Key.ToString();
			if (tecla == "S" || tecla == "s")
			{
				siContinuar = false;
			}
		}
		
		Console.WriteLine("Fin de bucle");
	}
	
	static void EjemploBucleFor()
	{
		// Un bucle for es un while que se sele usar con un contador
		// for ( <inicialización> ; <condición> ; <incremento> )
		// instrucción ; o bien un { bloque que se repite }
		for (int contador = 0 ; contador < 10 ; contador = contador + 1)
		{
			Console.WriteLine("Contador = " + contador);
		}
	}
	
	static void EjemploBucleDoWhile()
	{
		//Es como el bucle while, pero se ejecuta sí o sí
		// al menos una vez
		do 
		{
			Console.WriteLine("Al menos una vez");
		} while (false);
		
		bool siSalir = true;
		do 
		{
			Console.WriteLine("Hasta que la consición sea falsa");
			siSalir = ! siSalir;
		} while(!siSalir);
	}
}