using System;

public class Funciones {
	public static void Main () 
	{
		//Para usar la función ponemos el nombre y entre paréntesis los argumentos (parámetros)
		double y;
		double yy;
		string valorUsuario;
		double valUsu;
		Console.WriteLine("Introduzca valor de X: ");
		valorUsuario = Console.ReadLine();
		valUsu = Double.Parse(valorUsuario);
		y = FuncionLineal2x3(valUsu);
		yy = FuncionLinealxx1(valUsu);
		Console.WriteLine("Resultado 2x + 3 = " + y);
		Console.WriteLine("Resultado x^2 + 1 = " + yy);
	}
	//Forma de una función estática
	// <Modificador acceso> static <tipo dato resultado> <nombre de la función> (<tipo> parám1, <tipo> parám2, ...)
	// y luego entre llaves el cuerpo de la función
	// Con return podemos devolver un valor
	private static double FuncionLineal2x3 (double x)
	{
		return x * 2 + 1;
	}
	private static double FuncionLinealxx1 (double x)
	{
		return x * x + 1;
	}
}