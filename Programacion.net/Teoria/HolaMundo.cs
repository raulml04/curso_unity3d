// Al principio se ponen las importaciones
using System;

public class HolaMundo 
{
	static void Main() {
	Console.Beep();
	{
		Console.WriteLine("HolaMundo!");
		Console.WriteLine("¿Qué tal?");
		Console.ReadKey();
	}
	Console.WriteLine("Pues bien");
	Console.Beep();
	// tipo nombre ;
	byte unNumero; // byte de 0 a 255
	unNumero = 10;
	Console.WriteLine("El byte es " + unNumero);
	
	char unCaracter; // O bien 1 o 2 bytes
	unCaracter = 'A'; 	// comillas simples para caracteres simples
						// comillas dobles para textos de varios caracteres
	Console.WriteLine("El caracter es " + unCaracter);
	// un caracter también es un número
	unCaracter = (char) (65 + 4);	// un char es un número, pero para hacer 
							//la conversión hay que hacer "casting"
	Console.WriteLine("El caracter es " + unCaracter);
	
	}
	
}