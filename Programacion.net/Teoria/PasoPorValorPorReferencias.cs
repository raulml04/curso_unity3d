using System;

public class PasoPorValorPorReferencia
{
    static void Main()
    {
        Console.WriteLine("Paso por valor:");
        int y = 10;
        CambiarVariable(y);
        Console.WriteLine("Y = " + y);

        CambiarVariablePorReferencia(out y);
        Console.WriteLine("Y = " + y);
    }
    static void CambiarVariable(int x)
    {
        x = 20;
    }
    static void CambiarVariablePorReferencia(out int z)
    {
        z = 20;
    }
}