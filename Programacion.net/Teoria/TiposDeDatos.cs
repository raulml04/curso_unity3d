using System;

/*Estos son los tipos primitivos de C# 
(igual que en otros lenguajes de programación)

*/
public class TiposDeDatos {
	public static void Main() {
		byte unByte = 255;		// Nº entero entre 0 y 255, 8 bits, 2^8
		char unCaracter = 'A';	// Un caracter también es un nº
		int numEntero = 1000;	// Número entre -2.000.000.000 y 2.000.000.000
								// porque ocupa 4 byte. 2^32 = 4.000.000.000
		Console.WriteLine("Byte: " + unByte + " char: " + unCaracter);
		Console.WriteLine("Un char ocupa " + sizeof(char) + "bytes");
		Console.WriteLine("El entero vale " + numEntero);
		Console.WriteLine("Y ocupa " + sizeof(int) + "bytes");
		numEntero = 2000000000;	// 2 mil millones máx.
		Console.WriteLine("Ahora el entero vale " + numEntero);
		// Para guardar nº más largos:
		long enteroLargo = 5000000000; //8bytes
		Console.WriteLine("Entero largo vale " + enteroLargo);
		
		//Tipos de decimales: float (4bytes) y double (8 bytes) 
		// Precisión es de 	7-8 cifras.		15-16 cifras en TOTAL
		float numDecimal = 1.23456789F;
		Console.WriteLine("Entero decimal float vale " + numDecimal);
		//Para más precisión: double
		double numDoblePrecision = 12345.67890123456789;
		Console.WriteLine("Num decimal doble vale " + numDoblePrecision);
		
		// Para guardar Si/No, Verdadero/Falso, Cero/Uno...
		bool variableBooleana = true;
		Console.WriteLine("Variable Booleana vale " + variableBooleana);
		variableBooleana = numDecimal > 1000;	
		// Podemos guardar comparaciones, condiciones, etc...
		Console.WriteLine("Variable Booleana ahora es " + variableBooleana);
		variableBooleana = numDecimal <= 1000;
		Console.WriteLine("Variable Booleana ahora es " + variableBooleana);
		
		string cadenaDeTexto = "Pues eso, una cadena de texto";
		Console.WriteLine(cadenaDeTexto);
		Console.WriteLine(cadenaDeTexto + "que" + "permite concatenacion");
		// Lo que no permite son conversiones inválidas:
		string otroTxt = "" + 1;
	}

}