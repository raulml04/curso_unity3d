﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RaulSTS
{
    public class ControladorEscenas : MonoBehaviour
    {
        [Header("Propiedades ctrl escenas:")]
        public string nombreEscena;
        private void Start()
        {
            if (nombreEscena != "")
            {
                print("Arrancando escena " + nombreEscena);
                this.CargarEscena(nombreEscena);
            }
        }
        void Update()
        {
            if (Input.GetKey(KeyCode.Escape)) 
            {
                Application.Quit();
            }
        }

        public void CargarEscena(string escena) 
        {
            SceneManager.LoadScene(escena);
        }
    }
}
