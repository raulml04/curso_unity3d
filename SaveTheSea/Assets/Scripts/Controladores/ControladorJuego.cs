﻿using UnityEngine;
using System.Collections;

public class ControladorJuego: MonoBehaviour
{
    public GameObject prefabEnemigo;
    AparicionEnemigo[] apariciones;
    int enemigoActual;
    float timeInicio;

    // Use this for initialization
    void Start()
    {
        this.apariciones = new AparicionEnemigo[3];
        this.apariciones[0] = new AparicionEnemigo(2, 1);
        this.apariciones[1] = new AparicionEnemigo(-3, 6);
        this.apariciones[2] = new AparicionEnemigo(7, 10);
        enemigoActual = 0;
        timeInicio = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float tiempoActual = Time.time - timeInicio;
        if (enemigoActual < apariciones.Length)
        {
            // Si el tiempo que ha pasado es mayor que cuando se supone que debe
            // aparecer el enemigo actual, entonces...
            if (tiempoActual > apariciones[enemigoActual].tiempoInicio)
            {
                // Instanciamos el enemigo
                GameObject enemigo = GameObject.Instantiate(prefabEnemigo);
                float posX = apariciones[enemigoActual].posInicioX;
                enemigo.transform.position = new Vector3(posX, 18, 0);

                enemigoActual++;
            }
        }
    }
}
