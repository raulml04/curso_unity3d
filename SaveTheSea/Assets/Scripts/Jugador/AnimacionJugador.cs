﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionJugador : MonoBehaviour
{
    // Cambiar la escala eje X a +1 ó -1
    public Transform transfSpritesCaballito;
    // Cambiar el parámetro dirección
    public Animator animSprite;
    // Cambiar la velocidad
    public Animator animCtrlCuerpo;
    // Cambiar el clip de animación Legacy (antigua, heredada)
    public Animation animCuerpo;

    public void HaciaLaDerecha()
    {
        animSprite.SetInteger("direccion", +1);
        animCtrlCuerpo.speed = 2.5f;
        transfSpritesCaballito.localScale = new Vector3(-1, 1, 1);
        // Animación legacy (tradional) con transición por código
        animCuerpo.CrossFade("Nadar");
    }
    public void HaciaLaIzquierda()
    {
        animSprite.SetInteger("direccion", -1);
        // Hacer el flip 
        transfSpritesCaballito.localScale = new Vector3(1, 1, 1);
        animCuerpo.CrossFade("Nadar");
        animCtrlCuerpo.speed = 2.5f;
    }
    public void QuietoParao()
    {
        animSprite.SetInteger("direccion", 0);
        animCuerpo.CrossFade("Idle");
        animCtrlCuerpo.speed = 1;
    }
}
