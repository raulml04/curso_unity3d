﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        // Si se pulsa la tecla fecha derecha...
        if (Input.GetKey(KeyCode.RightArrow))
        {
            // Invocamos al movimiento
            GetComponent<MovimientoJugador>().Mover(1);
            // GetComponent<Animator>().SetBool("corriendo", true);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            // Invocamos al movimiento
            GetComponent<MovimientoJugador>().Mover(-1);
            // GetComponent<Animator>().SetBool("corriendo", true);
        }
    }
}
