﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{
    public float velocidad = 1f;
    

    /** Si dirección vale
     * +1 mueve derecha
     * -1 mueve izquierda
     * 0 no se mueve */
    public void Mover(int direccion)
    {
       // transform.position.x = 5; No se puede
        
        Vector3 posicion = GetComponent<Transform>().position;
        posicion.x = posicion.x + direccion * velocidad * Time.deltaTime; // * Time.time;
        transform.position = posicion;
        
        /*Debug.Log("Time = " + Time.time + ", dT = " + Time.deltaTime + ", X = " + posicion.x);

        EmularRetrasoCPU_GPU();*/
    }
    private void EmularRetrasoCPU_GPU()
    {
        int vueltas = Random.Range(100000, 50000000);
        for (int v = 0; v < vueltas; v++) 
        {
            v = v * 1 / 1;
        }
    }
}
